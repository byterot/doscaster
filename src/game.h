#pragma once

void game_init(void);
void game_quit(void);

void game_update(void);
void game_draw(void);
