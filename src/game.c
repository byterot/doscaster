#include "game.h"

#include "map.h"

#include "base/fpa.h" // Math 
#include "base/vga.h" // Graphics
#include "base/snd.h" // Sound
#include "base/inp.h" // Input

#include <stdio.h> // printf
#include <dos.h> // delay
#include <math.h>

#define PIXEL_WIDTH   4     // bytes per write to vga memory. set to: 1 (8 bits) 2 (16 bits) or 4 (32 bit)
#define SCREEN_WIDTH  320 / PIXEL_WIDTH
#define SCREEN_HEIGHT 170

#if (PIXEL_WIDTH == 1)
    unsigned volatile char *vga_start;
    unsigned volatile char *drawPtr;
#elif (PIXEL_WIDTH == 2)
    unsigned volatile short *vga_start;
    unsigned volatile short *drawPtr;
#else 
    unsigned volatile int *vga_start;
    unsigned volatile int *drawPtr;
#endif

static void put(
#if (PIXEL_WIDTH == 1)
    unsigned volatile char *p, unsigned char c
#elif (PIXEL_WIDTH == 2)
    unsigned volatile short *p, unsigned short c
#else
    unsigned volatile int *p, unsigned int c
#endif
){ *p = c; }

static void putc(
#if (PIXEL_WIDTH == 1)
    unsigned volatile char *p, unsigned char c
#elif (PIXEL_WIDTH == 2)
    unsigned volatile short *p, unsigned char c
#else
    unsigned volatile int *p, unsigned char c
#endif
)
{
#if (PIXEL_WIDTH == 1)
    *p = c;
#elif (PIXEL_WIDTH == 2)
    *p = c << 8 | c;
#else // assume 4 bytes. 
    *p = c << 24 | c << 16 | c << 8 | c;
#endif
}

Fixed plane_x, plane_y;
Fixed dir_x, dir_y;
Fixed player_x, player_y;

int mapX, mapY, stepX, stepY, hit, side, drawStart, drawEnd, wallHeight;    
Fixed cameraX, raydirX, raydirY, deltadistX, deltadistY, wallDist, sidedistX, sidedistY;

unsigned char ceiling_color = 20;
unsigned char floor_color = 24;

Fixed move_speed;
float turn_speed = 0.1f;

void game_init(void)
{
    printf("Initializing\n");
    vga_setmode(0x13);
    #if (PIXEL_WIDTH == 1)
        vga_start = vgaptr();
    #elif (PIXEL_WIDTH == 2)
        vga_start = vgaptr16();
    #else
        vga_start = vgaptr32();
    #endif

    player_x = fp_create(22);
    player_y = fp_create(12);
    dir_x = fp_create(-1);
    dir_y = fp_create(0);
    plane_x = fp_create(0);
    plane_y = fp_mul_i(fp_div_i(fp_create(1), 3), 2); // 0.66

    move_speed = fp_mul_i(fp_div_i(fp_create(1), 10) ,3); // 0.3

    drawPtr = vga_start + SCREEN_HEIGHT * SCREEN_WIDTH;
    int e = SCREEN_WIDTH * 200;
    unsigned char hud_color = 18;
    for(int i = SCREEN_WIDTH * SCREEN_HEIGHT; i < e; ++i)
    {
        putc(drawPtr, hud_color);
        ++drawPtr;
    }
}
void game_quit(void)
{
    vga_setmode(0x03);
}

static void turn(float amount)
{
    float old_dir_x = fp_float(dir_x);
    dir_x = fp_createf(old_dir_x * cos(amount) - fp_float(dir_y) * sin(amount));
    dir_y = fp_createf(old_dir_x * sin(amount) + fp_float(dir_y) * cos(amount));
    float old_plane_x = fp_float(plane_x);
    plane_x = fp_createf(old_plane_x * cos(amount) - fp_float(plane_y) * sin(amount));
    plane_y = fp_createf(old_plane_x * sin(amount) + fp_float(plane_y) * cos(amount));
}

void game_update(void)
{
   if(key_down(0x11)) // UP
   {
        if(!map_world[fp_int(fp_add(fp_mul(dir_x, move_speed), player_x))][fp_int(player_y)]) player_x = fp_add(fp_mul(dir_x, move_speed), player_x);
        if(!map_world[fp_int(player_x)][fp_int(fp_add(fp_mul(dir_y, move_speed), player_y))]) player_y = fp_add(fp_mul(dir_y, move_speed), player_y);
   }
   if(key_down(0x1F)) // DOWN
   {
        if(!map_world[fp_int(fp_sub(fp_mul(dir_x, move_speed), player_x))][fp_int(player_y)]) player_x = fp_sub(fp_mul(dir_x, move_speed), player_x);
        if(!map_world[fp_int(player_x)][fp_int(fp_sub(fp_mul(dir_y, move_speed), player_y))]) player_y = fp_sub(fp_mul(dir_y, move_speed), player_y);
        
        //if(!map_world[fp_int(fp_sub(fp_mul(dir_x, move_speed), player_x))][fp_int(player_y)]) player_x = fp_sub(fp_mul(dir_x, move_speed), player_x);
        //if(!map_world[fp_int(player_x)][fp_int(fp_sub(fp_mul(dir_y, move_speed), player_y))]) player_y = fp_sub(fp_mul(dir_y, move_speed), player_y);
   }
   if(key_down(0x1E)) // LEFT
   {
       turn(turn_speed);
   }
   if(key_down(0x20)) // RIGHT
   {
       turn(-turn_speed);
   }
}


void game_draw(void)
{
    for(int x = 0; x < SCREEN_WIDTH; ++x)
    {
        mapX = fp_int(player_x);    
        mapY = fp_int(player_y);
        
        //cameraX = 2 * (x * PIXEL_WIDTH) / (Fixed)320 - 1;
        cameraX = fp_sub_i(fp_div_i(fp_mul_i(fp_create(x * PIXEL_WIDTH), 2), 320), 1);
        
        //printf("%d cameraX: %f\n", x, fp_float(cameraX));
        //delay(100);
        //raydirX = dir_x + plane_x * cameraX;
        //raydirY = dir_y + plane_y * cameraX;
        raydirX = fp_add(dir_x, fp_mul(plane_x, cameraX));
        raydirY = fp_add(dir_y, fp_mul(plane_y, cameraX));

        if(fp_zero(raydirX) || fp_zero(raydirY))
            continue;

        //printf("%d rayDir x,y: %f, %f\n", x, fp_float(raydirX), fp_float(raydirY));
        //delay(100);
        //deltadistX = abs(1 / raydirX);
        //deltadistY = abs(1 / raydirY);
        deltadistX = fp_abs(fp_div_i_by(1, raydirX));
        deltadistY = fp_abs(fp_div_i_by(1, raydirY));
       
        //printf("%d delta x,y: %f, %f\n", x, fp_float(deltadistX), fp_float(deltadistY));
        //delay(100);


        if(fp_negative(raydirX)) {
            stepX = -1;
            //sidedistX = (player_x - mapX) * deltadistX;
            sidedistX = fp_mul(fp_sub_i(player_x, mapX), deltadistX);
        } else {
            stepX = 1;
            //sidedistX = (mapX + 1.0 - player_x) * deltadistX;
            sidedistX = fp_mul(fp_sub_i_by(mapX + 1, player_x), deltadistX);
        }
        if(fp_negative(raydirY)) {
            stepY = -1;
            //sidedistY = (player_y - mapY) * deltadistY;
            sidedistY = fp_mul(fp_sub_i(player_y, mapY), deltadistY);
        } else {
            stepY = 1;
            //sidedistY = (mapY + 1.0 - player_y) * deltadistY;
            sidedistY = fp_mul(fp_sub_i_by(mapY + 1, player_y), deltadistY);
        }

        //printf("%d side x,y: %f, %f\n", x, fp_float(sidedistX), fp_float(sidedistY));
        //delay(500);

        while(!map_world[mapX][mapY])
        {
            if(fp_less(sidedistX, sidedistY))
            {
                //sidedistX += deltadistX;
                sidedistX = fp_add(sidedistX, deltadistX);
                mapX += stepX;
                side = 0;
            }
            else
            {
                //sidedistY += deltadistY;
                sidedistY = fp_add(sidedistY, deltadistY);
                mapY += stepY;
                side = 1;
            }
            hit = map_world[mapX][mapY];
        }
        
        //if(side == 0) walldist = abs((mapx - player_x + (1 - stepx) / 2) / raydirx);
        //else          walldist = abs((mapy - player_y + (1 - stepy) / 2) / raydiry);

        if(side == 0) wallDist = fp_abs(fp_div(fp_add(fp_sub_i_by(mapX, player_x), fp_div_i(fp_create(1 - stepX), 2)), raydirX));
        else          wallDist = fp_abs(fp_div(fp_add(fp_sub_i_by(mapY, player_y), fp_div_i(fp_create(1 - stepY), 2)), raydirY));
        
        //printf("%d walldist: %f\n", x, fp_float(wallDist));
        //delay(1000);
        //wallHeight = (int)(SCREEN_HEIGHT / wallDist);
        wallHeight = fp_zero(wallDist) ? SCREEN_HEIGHT : fp_int(fp_div_i_by(SCREEN_HEIGHT, wallDist));

        drawStart = -wallHeight / 2 + SCREEN_HEIGHT / 2;
        if(drawStart < 0) drawStart = 0;
        drawEnd = drawStart + wallHeight;
        if(drawEnd >= SCREEN_HEIGHT) drawEnd = SCREEN_HEIGHT - 1;

        drawPtr = vga_start + x;
        
        unsigned char c = side == 0 ? map_world[mapX][mapY] : map_world[mapX][mapY] + 8;
        for(int y = 0; y < drawStart; ++y)
        {
            putc(drawPtr, ceiling_color);
            drawPtr += SCREEN_WIDTH;
        }
        for(int y = drawStart; y < drawEnd; ++y)
        {
            putc(drawPtr, c);
            drawPtr += SCREEN_WIDTH;
        }
        for(int y = drawEnd; y < SCREEN_HEIGHT - 1; ++y)
        {
            putc(drawPtr, floor_color);
            drawPtr += SCREEN_WIDTH;
        }
    }
    vga_sync();
}
