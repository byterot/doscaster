#include <sys/nearptr.h>
#include <stdio.h>

#include "base/inp.h"
#include "game.h"

#include "base/fpa.h"

int main()
{    
       Fixed f = fp_create(2);
       Fixed f2 = fp_create(20);

       Fixed f3 = fp_add(f, f2);
        
       Fixed f4 = fp_div_i_by(1, f3);

       printf("1 / %f = %f\n", fp_float(f3), fp_float(f4));

       printf("%d + %d = %d\n",fp_int(f),fp_int(f2),fp_int(f3));

       f3 = fp_mul(f,f2);    
       printf("%d x %d = %d\n",fp_int(f),fp_int(f2),fp_int(f3));

       f3 = fp_div(f,f2);
       printf("%d / %d = %f\n",fp_int(f),fp_int(f2),fp_float(f3));

       Fixed f5 = fp_div_i_by(1, fp_create(10));
       printf("1 / 10 = %f\n", fp_float(f5));

       printf("1 / 0.1 = %f\n", fp_float(fp_div_i_by(1,f5 )));
     
//    return 0;
    if(!__djgpp_nearptr_enable())
        return 1;
    
    game_init(); 
    keyboard_bind();

    while(1)
    {
        keyboard_fetch();
        if(key_pressed(0x1)) // ESCAPE
            break;

        game_update();
        game_draw();
    }

    keyboard_unbind();
    game_quit();
    __djgpp_nearptr_disable();
}
