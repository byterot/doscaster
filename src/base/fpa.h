#pragma once
// Fixed Point Arithmetic resides here. 
// rnlf: "Where we're going, we don't need no stinking FPU."

// Radix 16.16
#define FP_RADIX_SHIFT 16
// All MSB zeroed. All LSB 1
#define FP_RADIX_MASK (1 << FP_RADIX_SHIFT) - 1
#define FP_RADIX_SCALE (1 << (FP_RADIX_SHIFT))


typedef int fp_base;
typedef long long fp_overflow;

typedef struct {
  fp_base value;
} Fixed;

// Initialization
static inline Fixed fp_create(fp_base i)
{
  Fixed r = {i << FP_RADIX_SHIFT};
  return r;
}

static inline Fixed fp_createf(float f)
{
    Fixed r = {f * FP_RADIX_SCALE};
    return r;
}

// Result casting
static inline fp_base fp_int(Fixed f)
{
  return f.value >> FP_RADIX_SHIFT;
}

static inline float fp_float(Fixed f)
{
  return (float)f.value / FP_RADIX_SCALE;
}

// Addition
static inline Fixed fp_add(Fixed a, Fixed b)
{
  Fixed r = {a.value + b.value};
  return r;
}

static inline Fixed fp_add_i(Fixed a, int i)
{
    return fp_add(a, fp_create(i));
}

static inline Fixed fp_add_i_by(int i, Fixed a)
{
    return fp_add(fp_create(i), a);
}

// Substraction
static inline Fixed fp_sub(Fixed a, Fixed b)
{
  Fixed r = {a.value - b.value};
  return r;
}

static inline Fixed fp_sub_i(Fixed a, int i)
{
    return fp_sub(a, fp_create(i));
}

static inline Fixed fp_sub_i_by(int i, Fixed a)
{
    return fp_sub(fp_create(i), a);
}

// Multiplication
static inline Fixed fp_mul(Fixed a, Fixed b)
{
  Fixed r = {((fp_overflow)a.value * (fp_overflow)b.value) / FP_RADIX_SCALE};
  return r;
}

static inline Fixed fp_mul_i(Fixed a, int i)
{
    return fp_mul(a, fp_create(i));
}

// Division
static inline Fixed fp_div(Fixed a, Fixed b)
{
  Fixed r = {((fp_overflow)a.value * FP_RADIX_SCALE) / b.value};
  return r;
}

static inline Fixed fp_div_i(Fixed a, int i)
{
    return fp_div(a, fp_create(i));
}

static inline Fixed fp_div_i_by(int i, Fixed a)
{
    return fp_div(fp_create(i), a);
}

// Comparison
static inline int fp_less(Fixed a, Fixed b)
{
    return a.value < b.value;
}
static inline int fp_greater(Fixed a, Fixed b)
{
    return a.value > b.value;
}
static inline int fp_equal(Fixed a, Fixed b)
{
    return a.value == b.value;
}
static inline int fp_not_equal(Fixed a, Fixed b)
{
    return a.value != b.value;
}
static inline int fp_greater_equal(Fixed a, Fixed b)
{
    return a.value >= b.value;
}
static inline int fp_less_equal(Fixed a, Fixed b)
{
    return a.value <= b.value;
}
// Utility
static inline Fixed fp_neg(Fixed a)
{
    Fixed r = {-a.value};
    return r;
}
static inline Fixed fp_abs(Fixed a)
{
    return a.value > 0 ? a : fp_neg(a);
}
static inline int fp_negative(Fixed a)
{
    return a.value < 0;
}
static inline int fp_positive(Fixed a)
{
    return a.value > 0;
}
static inline int fp_zero(Fixed a)
{
    return a.value == 0;
}
static inline Fixed fp_pi()
{
    return fp_createf(3.14159265358979323846);
}
static inline Fixed fp_deg(Fixed a)
{
    return fp_div(fp_mul_i(a, 180), fp_pi());
}
static inline Fixed fp_rad(Fixed a)
{
    return fp_div_i(fp_mul(a,fp_pi()), 180);
}
