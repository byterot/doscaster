// Input handler
// <rnlf> when you find an IRQ list, bitrot, add 8 if the IRQ number is 7 or lower, and 0x70 if it's 8 or higher -> that's the interrupt vector you have to use

#include <string.h> // memset
#include <dpmi.h>
#include <dos.h>
#include <pc.h> // port control
#include <go32.h>
#include <stdio.h>

#define KEYBOARD_INTERRUPT 9
#define KEYBOARD_DATA_PORT 0x60

#define SCANCODE_PREFIX 0xE0    // prefix sent when scancode is 16 bit
#define SCANCODE_OFFSET 0x100   // ??
#define SCANCODE_MAX    0x200   // highest scancode we want to listen for

#define QUEUE_SIZE      64

static unsigned char    input_queue[QUEUE_SIZE]; // Interrupt controlled
static unsigned int     queue_position;

static unsigned char    shadow_queue[QUEUE_SIZE]; // Used by fetch()

static unsigned char    keys_down[SCANCODE_MAX];
static unsigned char    keys_pressed[SCANCODE_MAX];
static unsigned char    keys_released[SCANCODE_MAX];

static _go32_dpmi_seginfo old_isr;
static _go32_dpmi_seginfo new_isr;

static void keyboard_isr(void) 
{
    if(queue_position < QUEUE_SIZE)
    {
        input_queue[queue_position] = inp(KEYBOARD_DATA_PORT);
        ++queue_position;
    }
    
    // End of interrupt
    outportb(0x20, 0x20);   // 0x20 = PIC 1
                            // 0x20 = End of Interrupt
}

void keyboard_bind(void)
{
    // Hook into IRQ 1
    _go32_dpmi_get_protected_mode_interrupt_vector(KEYBOARD_INTERRUPT, &old_isr);
    new_isr.pm_offset = (int)keyboard_isr;
    new_isr.pm_selector = _go32_my_cs();
    _go32_dpmi_allocate_iret_wrapper(&new_isr); 
    _go32_dpmi_set_protected_mode_interrupt_vector(KEYBOARD_INTERRUPT, &new_isr);

    // Initialize the queue
    queue_position = 0;
    memset(keys_down,       0, sizeof(keys_down));
    memset(keys_pressed,    0, sizeof(keys_pressed));
    memset(keys_released,   0, sizeof(keys_released));
}

void keyboard_unbind(void)
{
    // Release interrupt, do some cleanup.
    _go32_dpmi_set_protected_mode_interrupt_vector(KEYBOARD_INTERRUPT, &old_isr);
    _go32_dpmi_free_iret_wrapper(&new_isr);
}

void keyboard_fetch()
{
    // Copy input queue into shadow queue
    memcpy(shadow_queue, input_queue, queue_position);
    // Reset queue up until queue position
    memset(input_queue, 0, queue_position);
    int queue_length = queue_position;
    queue_position = 0;

    // If only first byte of two-byte scancode was received,
    // keep the first byte in the input queue
    if(shadow_queue[queue_length-1] == SCANCODE_PREFIX)
    {
        input_queue[0] = SCANCODE_PREFIX;
        queue_position = 1;
        --queue_length;
    }

    memset(keys_pressed,  0, sizeof(keys_pressed));
    memset(keys_released, 0, sizeof(keys_released));

    for(int i = 0; i < queue_length; ++i)
    {
        int scancode;
        if(shadow_queue[i] == SCANCODE_PREFIX)
        {
            ++i;
            scancode = SCANCODE_PREFIX + (shadow_queue[i] & 0x7F);
        } 
        else
        {
            scancode = shadow_queue[i] & 0x7F;
        }

        bool released = (shadow_queue[i] & 0x80) != 0;

        if(released && keys_down[scancode])
        {
            keys_released[scancode] = true;
        }
        if(!released && !keys_down[scancode])
        {
            keys_pressed[scancode] = true;
        }
        keys_down[scancode] = !released;
    }
}

unsigned char key_down(int scancode)
{
    return keys_down[scancode];
}

unsigned char key_pressed(int scancode)
{
    return keys_pressed[scancode];
}

unsigned char key_released(int scancode)
{
    return keys_released[scancode];
}
