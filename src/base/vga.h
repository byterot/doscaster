#pragma once

// calls interrupt 0x10 to set graphics mode
void vga_setmode(int mode);

// sets one color in the vga palette to the specified color
void vga_setcolor(int color, unsigned char r, unsigned char g, unsigned char b);

// sync with vertical retrace
void vga_sync();

// clear vga memory
void vga_clear(unsigned char color);

// returns an 8 bits pointer to vga memory
unsigned volatile char *vgaptr();
// returns a 16 bits pointer to vga memory
unsigned volatile short *vgaptr16();
// returns a 32 bits pointer to vga memory
unsigned volatile int *vgaptr32();

