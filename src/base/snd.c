#include "snd.h"
#include "pc.h"

void beep(unsigned int frequency)
{
    unsigned int div;
    unsigned char tmp;

    div = 1193180 / frequency;
    
    outportb(0x43, 0xb6);
    outportb(0x42, (unsigned char)div);
    outportb(0x42, (unsigned char)(div >> 8));

    tmp = inportb(0x61);
    if(tmp != (tmp | 3))
       outportb(0x61, tmp | 3);
}

void nobeep()
{
    unsigned char tmp = inportb(0x61) & 0xFC;

    outportb(0x61, tmp);
}

