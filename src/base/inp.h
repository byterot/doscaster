// Input handle
#pragma once

void keyboard_fetch();

void keyboard_bind(void);
void keyboard_unbind(void);

unsigned char key_down(int scancode);
unsigned char key_pressed(int scancode);
unsigned char key_released(int scancode);
